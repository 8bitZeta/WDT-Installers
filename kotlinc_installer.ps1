$releaseList = Invoke-WebRequest -Uri "https://api.github.com/repos/JetBrains/kotlin/releases" | ConvertFrom-Json

If (!(test-path "$HOME\.utils"))
{
    md "$HOME\.utils"
}

$nameList = @()

foreach ($release in $releaseList)
{
	if (-Not $release.name.Contains('-'))
	{
   		$nameList += $release.name
   	}
}
$userVersion = $nameList[0].Replace("Kotlin ", "")

Write-Host "Please wait..."

Foreach ($release in $releaseList)
{
    Foreach ($entry in $release.assets)
    {
        if ($entry.browser_download_url.Contains("kotlin-compiler-" + $userVersion + ".zip"))
        {
            if (!$entry.browser_download_url.Contains("sha256"))
            {
                echo $entry.browser_download_url
                Start-BitsTransfer -Source $entry.browser_download_url -Destination "$HOME/.utils/kotlinc.zip"
                Expand-Archive -LiteralPath "$HOME/.utils/kotlinc.zip" -DestinationPath "$HOME/.utils/" -Force
                $old = (Get-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Environment" -Name path).path
                $new  =  "$old;$HOME\.utils\kotlinc\bin"
                if (!$old.Contains(".utils\kotlinc\bin"))
                {
                	Set-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Environment" -Name path -Value $new
               	}
            }
        }
    }
}

Write-Host "kotlinc has been installed!"
