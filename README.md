<div align="center">
  <h1>WDT-Installers</h1>

  <p>
    Collection of Installation Scripts
  </p>
  <br>
</div>


## w64devkit (`gcc` and other tools)

### Information
https://github.com/skeeto/w64devkit

### Installation
```pwsh
powershell -Exec ByPass -NoProfile -c "(New-Object Net.WebClient).DownloadString('https://codeberg.org/8bitZeta/WDT-Installers/raw/branch/main/w64devkit_installer.ps1') | IEX"
```

## llvm-mingw (native `clang` for Windows)

### Information
https://github.com/mstorsjo/llvm-mingw

### Installation
```pwsh
powershell -Exec ByPass -NoProfile -c "(New-Object Net.WebClient).DownloadString('https://codeberg.org/8bitZeta/WDT-Installers/raw/branch/main/llvm-mingw_installer.ps1') | IEX"
```

## kotlinc (Compiler for Kotlin)

### Information
https://kotlinlang.org/docs/command-line.html

### Installation
```pwsh
powershell -Exec ByPass -NoProfile -c "(New-Object Net.WebClient).DownloadString('https://codeberg.org/8bitZeta/WDT-Installers/raw/branch/main/kotlinc_installer.ps1') | IEX"
```


