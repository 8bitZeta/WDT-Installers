$releaseList = Invoke-WebRequest -Uri "https://api.github.com/repos/skeeto/w64devkit/releases" | ConvertFrom-Json

If (!(test-path "$HOME\.utils"))
{
    md "$HOME\.utils"
}

$nameList = @()

Foreach ($release in $releaseList)
{
    $nameList += $release.name
}
$userVersion = $nameList[0].Replace("v", "")

Write-Host "Please wait..."

Foreach ($release in $releaseList)
{
    Foreach ($entry in $release.assets)
    {
        if ($entry.browser_download_url.Contains("w64devkit-" + $userVersion))
        {
            if (!$entry.browser_download_url.Contains("sig"))
            {
                Start-BitsTransfer -Source $entry.browser_download_url -Destination "$HOME/.utils/w64devkit.zip"
				Expand-Archive -LiteralPath "$HOME/.utils/w64devkit.zip" -DestinationPath "$HOME/.utils/" -Force
				$old = (Get-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Environment" -Name path).path
				$new  =  "$old;$HOME\.utils\w64devkit\bin" 
				if (!$old.Contains(".utils\w64devkit\bin"))
				{
					Set-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Environment" -Name path -Value $new
				}
			}
        }
    }
}

Write-Host "w64devkit has been installed!"